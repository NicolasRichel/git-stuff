# My Git Stuff

This repository contains some git stuff that I found to be useful, or just fun !

## Content

 - **config/**
   - [.gitconfig](./config/.gitconfig) :
     my global git config (so I can keep my aliases definitions)
   - [config.md](./config/README.md) :
     configuration tips at the repository level
 - **scripts/**
   - [scripts.md](./scripts/README.md) :
     description of some useful scripts

